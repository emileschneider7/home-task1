import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

class ApplicationTest {
    private MockOrderServiceClient mockOrderServiceClient = new MockOrderServiceClient();
    Application application = new Application();

    void setUp() {
        application.orderServiceClient = mockOrderServiceClient;
        application.deliveryTimes = List.of(LocalTime.of(10, 30), LocalTime.of(12, 30), LocalTime.of(18, 30));

    }

    @Test
    void testTuesday() {
        setUp();

        var res = application.availableDeliveryTimes(LocalDate.of(2021, 12, 7));
        System.out.println(res);
        assert(res.size() == 2);
        assert(res.contains(LocalTime.of(12, 30)));
        assert(res.contains(LocalTime.of(18, 30)));
    }

    @Test
    void testMonday() {
        setUp();
        var res = application.availableDeliveryTimes(LocalDate.of(2021, 12, 6));
        System.out.println(res);
        assert(res.size() == 2);
        assert(res.contains(LocalTime.of(10, 30)));
        assert(res.contains(LocalTime.of(12, 30)));
    }

    @Test
    void noOrders() {
        setUp();
        var res = application.availableDeliveryTimes(LocalDate.of(2021, 12, 8));
        assert(res.size() == 3);
        assert(res.contains(LocalTime.of(10, 30)));
        assert(res.contains(LocalTime.of(12, 30)));
        assert(res.contains(LocalTime.of(18, 30)));
    }
}
