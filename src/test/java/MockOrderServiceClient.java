import ee.cleankitchen.orderservice.OrderServiceClient;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MockOrderServiceClient implements OrderServiceClient {

    @Override
    public List<Map<String, Object>> getBy(LocalDate date) {
        if (LocalDate.of(2021, 12, 7).equals(date)) {
            return List.of(
                    Map.of("date", LocalDate.of(2021, 12, 7), "time", LocalTime.of(10, 30), "orderId", "123", "customerId", " a - 111 "),
                    Map.of("date", LocalDate.of(2021, 12, 7), "time", LocalTime.of(10, 30), "orderId", "124", "customerId", " a - 112 ")
            );
        } else if(LocalDate.of(2021, 12, 6).equals(date)) {
            return List.of(
                    Map.of("date", LocalDate.of(2021, 12, 6), "time", LocalTime.of(12, 30), "orderId", "123", "customerId", " a - 111 "),
                    Map.of("date", LocalDate.of(2021, 12, 6), "time", LocalTime.of(12, 30), "orderId", "123", "customerId", " a - 111 "),
                    Map.of("date", LocalDate.of(2021, 12, 6), "time", LocalTime.of(12, 30), "orderId", "123", "customerId", " a - 111 "),
                    Map.of("date", LocalDate.of(2021, 12, 6), "time", LocalTime.of(18, 30), "orderId", "123", "customerId", " a - 111 "),
                    Map.of("date", LocalDate.of(2021, 12, 6), "time", LocalTime.of(18, 30), "orderId", "123", "customerId", " a - 111 "),
                    Map.of("date", LocalDate.of(2021, 12, 6), "time", LocalTime.of(18, 30), "orderId", "123", "customerId", " a - 111 "),
                    Map.of("date", LocalDate.of(2021, 12, 6), "time", LocalTime.of(18, 30), "orderId", "123", "customerId", " a - 111 ")
            );
        } else {
            return new ArrayList<>();
        }
    }
}
