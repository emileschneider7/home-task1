import ee.cleankitchen.orderservice.OrderServiceClient;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Application {

    private int limitForMonday = 4;
    private int limitForOtherDays = 2;

    List<LocalTime> deliveryTimes;
    OrderServiceClient orderServiceClient;
    Map<DayOfWeek, Integer> slotsPerDelivery = Map.of(
            DayOfWeek.MONDAY, 4,
            DayOfWeek.TUESDAY, 2,
            DayOfWeek.WEDNESDAY, 2,
            DayOfWeek.THURSDAY, 2,
            DayOfWeek.FRIDAY, 2,
            DayOfWeek.SATURDAY, 0,
            DayOfWeek.SUNDAY, 0);

    public List<LocalTime> availableDeliveryTimes(LocalDate date) {
        List<LocalTime> openDeliveryTimes = new ArrayList<>();
        Map<LocalTime, Long> usedDeliverySlots = new HashMap<>();
        DayOfWeek day = date.getDayOfWeek();

        int availableSlotsPerTime = slotsPerDelivery.get(day);

        List<Map<String, Object>> deliveries = orderServiceClient.getBy(date);
        for (LocalTime time : deliveryTimes) {
            //how many delivery are already in any give timeslot
            long count = deliveries.stream().filter(delivery -> delivery.get("time").equals(time)).count();
            usedDeliverySlots.put(time, count);
        }

        for (var entry : usedDeliverySlots.entrySet()) {
            if (entry.getValue() < availableSlotsPerTime) {
                openDeliveryTimes.add(entry.getKey());
            }
        }
        return openDeliveryTimes;
    }


}
